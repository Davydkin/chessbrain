import chess
import chess.engine
from stockfish import Stockfish
from common import *
import time
import threading


def main():

    engine = chess.engine.SimpleEngine.popen_uci("engines/Stockfish/src/stockfish")
    engine.configure({"Threads": 8})

    board = chess.Board()
    analysis = engine.analysis(board)

    while True:
        try:
            cmd = input()
            if cmd == '':
                print(analysis.info)
                print('Depth', analysis.info['depth'])
                print('Selective depth', analysis.info['seldepth'])
            elif cmd == 'b':
                move_uci = analysis.info['pv'][0]
                if move_uci in board.legal_moves:
                    board.push(move_uci)
                    print('pushed')
                    analysis = engine.analysis(board)
                else:
                    print('wrong move')
                print(board)
            elif cmd == 'bb':
                print(board)
            else:
                move_uci = chess.Move.from_uci(cmd)
                if move_uci in board.legal_moves:
                    board.push(move_uci)
                    print('pushed')
                    analysis = engine.analysis(board)
                else:
                    print('wrong move')
        except ValueError:
            print('wrong move')
        except KeyboardInterrupt:
            break

    engine.quit()
    engine.close()


if __name__ == '__main__':
    main()
