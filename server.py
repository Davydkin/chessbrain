import cherrypy
import os.path
import chess
import random
import threading
import chess.svg
from common import *


cherrypy.config.update({
    'server.socket_host': '0.0.0.0',  # 127.0.0.1 is default
    'server.socket_port': 8080,  # 8080 is default
    'server.thread_pool': 100,  # 10 is default
    'tools.trailing_slash.on': False,  # True is default
    'tools.staticdir.on': True,
    'tools.staticdir.dir': os.path.join(os.path.dirname(os.path.realpath(__file__)), 'landing'),
})

config_api = {
    '/api': {
        'tools.response_headers.on': True,
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8080
    }
}


class Landing(object):
    def __init__(self):
        with open('static/index.html', 'r') as f:
            self._index = f.read()

    @cherrypy.expose
    def index(self):
        return self._index

    def _make_engine(self):
        return EngineLc0()

    @cherrypy.expose
    def turn(self, board=None, move=None):
        engine = self._make_engine()
        result_dict = {'message': ''}

        if board is None:
            board = chess.Board()
        else:
            try:
                board = chess.Board(board)
            except ValueError:
                board = chess.Board()
        if move is not None:
            try:
                move_uci = chess.Move.from_uci(move)
                if move_uci in board.legal_moves:
                    board.push(move_uci)
                    if board.is_checkmate():
                        result_dict['message'] = 'Шах и мат'
                    if board.is_game_over():
                        result_dict['message'] = 'Игра окончена'

                    engine_move = engine.best_move(board)
                    board.push(engine_move)
                    if board.is_checkmate():
                        result_dict['message'] = 'Шах и мат'
                    if board.is_game_over():
                        result_dict['message'] = 'Игра окончена'
                else:
                    raise ValueError
            except ValueError:
                result_dict['message'] = f'Недопустимый ход {move}'

        with open('static/turn.html', 'r') as f:
            text = f.read()
        result_dict['board_svg'] = chess.svg.board(
            board,
            size=500,
            orientation=chess.WHITE if board.turn else chess.BLACK
        )
        result_dict['board'] = board.epd()
        engine.close()
        return text.format(**result_dict)

    @cherrypy.expose
    def turn_black(self, board=None, move=None):
        result_dict = {'message': ''}
        engine = self._make_engine()

        board = chess.Board()
        board.push(engine.best_move(board))

        with open('static/turn.html', 'r') as f:
            text = f.read()
        result_dict['board_svg'] = chess.svg.board(board, size=500, orientation=chess.BLACK)
        result_dict['board'] = board.epd()
        engine.close()
        return text.format(**result_dict)


class ApiServer(object):
    def _make_engine(self):
        return EngineRandom()

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self, board, move):
        engine = self._make_engine()
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
        }

        if board is None:
            board = chess.Board()
        else:
            try:
                board = chess.Board(board)
            except ValueError:
                result_dict['message'] = 'Ошибка в состоянии доски'
                return result_dict

        try:
            move_uci = chess.Move.from_uci(move)
            if move_uci in board.legal_moves:
                board.push(move_uci)
                if board.is_checkmate():
                    result_dict['message'] = 'Шах и мат'
                    result_dict['checkmate'] = True
                    result_dict['game_end'] = True
                elif board.is_game_over():
                    result_dict['message'] = 'Игра окончена'
                    result_dict['checkmate'] = False
                    result_dict['game_end'] = True

                else:
                    engine_move = engine.best_move(board)
                    board.push(engine_move)
                    if board.is_checkmate():
                        result_dict['message'] = 'Шах и мат'
                        result_dict['checkmate'] = True
                        result_dict['game_end'] = True
                    elif board.is_game_over():
                        result_dict['message'] = 'Игра окончена'
                        result_dict['checkmate'] = False
                        result_dict['game_end'] = True
            else:
                raise ValueError
        except ValueError:
            result_dict['message'] = f'Недопустимый ход {move}'

        result_dict['board'] = board.epd()
        engine.close()
        return result_dict

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def init_black(self):
        engine = self._make_engine()
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
        }

        board = chess.Board()

        engine_move = engine.best_move(board)
        board.push(engine_move)

        result_dict['board'] = board.epd()
        engine.close()
        return result_dict

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def init_white(self):
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
        }

        board = chess.Board()

        result_dict['board'] = board.epd()
        return result_dict


class Api2Server:
    def __init__(self):
        self.pre_session = GameSession()
        self.sessions = {}
        self._working = True
        self._thread = threading.Thread(target=self._session_cleaner)
        self._thread.start()

    def _session_cleaner(self):
        while self._working:
            time.sleep(1)
            session_names = list(self.sessions.keys())
            for session_name in session_names:
                session = self.sessions[session_name]
                if time.time() - session.last_ping > 10:
                    print('Cleaning session ' + session_name)
                    session.close()
                    del(self.sessions[session_name])

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def index(self, session_id, move):
        return self.sessions[session_id].player_move(move)

    @cherrypy.expose
    def ping(self, session_id):
        self.sessions[session_id].last_ping = time.time()
        try:
            print('Depth', self.sessions[session_id].analysis.info['depth'])
        except:
            pass
        return str(self.sessions[session_id].analysis.info)

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def cancel_move(self, session_id):
        return self.sessions[session_id].cancel_move()

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def init_black(self):
        if len(self.sessions) > 3:
            return {}
        session_id = str(time.time()) + str(random.random())
        self.sessions[session_id] = self.pre_session
        self.sessions[session_id].last_ping = time.time()
        self.pre_session = GameSession()
        result_dict = self.sessions[session_id].init_player_is_black()
        result_dict['session_id'] = session_id
        return result_dict

    @cherrypy.expose
    @cherrypy.tools.json_out()
    def init_white(self):
        if len(self.sessions) > 3:
            return {}
        session_id = str(time.time()) + str(random.random())
        self.sessions[session_id] = self.pre_session
        self.sessions[session_id].last_ping = time.time()
        self.pre_session = GameSession()
        result_dict = self.sessions[session_id].init_player_is_white()
        result_dict['session_id'] = session_id
        return result_dict

    def __del__(self):
        self._working = False


if __name__ == "__main__":
    cherrypy.quickstart(Api2Server(), '/api', config_api)
