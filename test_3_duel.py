import time
from common import *
import multiprocessing as mp


N_TEST = 16


def duel(engine_white_creator, engine_black_creator, result):
    e_white = engine_white_creator()
    e_black = engine_black_creator()

    board = chess.Board()
    while True:
        # White
        board.push(e_white.best_move(board))
        if board.is_checkmate():
            result.value = 1
            break
        if board.is_game_over():
            result.value = 0
            break

        # black
        board.push(e_black.best_move(board))
        if board.is_checkmate():
            result.value = 2
            break
        if board.is_game_over():
            result.value = 0
            break

    e_white.close()
    e_black.close()


def duel_series(e1, e2):
    result = [mp.Value('d', -1)] * N_TEST
    threads = []

    t1 = time.time()
    for i in range(N_TEST):
        if i % 2 == 0:
            t = mp.Process(target=duel, args=(e1, e2, result[i]))
        else:
            t = mp.Process(target=duel, args=(e2, e1, result[i]))
        threads.append(t)
        t.start()
    for t in threads:
        t.join()

    e1_wins, e2_wins, draws = 0, 0, 0
    for i, rc in enumerate(result):
        if i % 2 == 0:
            if rc.value == 1:
                e1_wins += 1
            elif rc.value == 2:
                e2_wins += 1
            else:
                draws += 1
        else:
            if rc.value == 1:
                e2_wins += 1
            elif rc.value == 2:
                e1_wins += 1
            else:
                draws += 1
    print(f'Done in {time.time() - t1}s')

    print('\n')
    print(e1_wins, ' - ', e2_wins, '             draws:', draws)


def main():
    duel_series(EngineStockfish, EngineKoivisto)


if __name__ == '__main__':
    main()
