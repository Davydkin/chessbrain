import chess
import chess.engine
from stockfish import Stockfish


def main():
    e_stockfish = Stockfish(path='engines/stockfish_14.1_linux_x64/stockfish_14.1_linux_x64')

    board = chess.Board()
    e_ethereal = chess.engine.SimpleEngine.popen_uci('engines/Ethereal-13.00/src/Ethereal')
    e_koivisto = chess.engine.SimpleEngine.popen_uci('engines/Koivisto/bin/Koivisto_8.6-x64-linux-native')

    print(e_stockfish.get_best_move())

    info = e_ethereal.analyse(board, chess.engine.Limit(depth=1))
    print(info['pv'][0])

    info = e_koivisto.analyse(board, chess.engine.Limit(depth=1))
    print(info['pv'][0])

    e_ethereal.close()
    e_koivisto.close()


if __name__ == '__main__':
    main()
