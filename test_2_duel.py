import chess
import chess.engine
import time
from itertools import combinations
import multiprocessing as mp


N_TEST = 16


def find_best_move(e, board):
    info = e.analyse(board, chess.engine.Limit(depth=10))
    if 'pv' in info:
        return info['pv'][0]
    raise StopIteration


def duel(e1_name, e2_name, result, i):
    engines = {
        'stockfish': chess.engine.SimpleEngine.popen_uci('engines/stockfish_14.1_linux_x64/stockfish_14.1_linux_x64'),
        'ethereal': chess.engine.SimpleEngine.popen_uci('engines/Ethereal-13.00/src/Ethereal'),
        'koivisto': chess.engine.SimpleEngine.popen_uci('engines/Koivisto/bin/Koivisto_8.6-x64-linux-native'),
    }
    e1 = engines[e1_name]
    e2 = engines[e2_name]
    board = chess.Board()
    c = 0
    while True:
        c += 1
        board.push(find_best_move(e1, board))
        if board.is_checkmate():
            result[i] = 1
            break
        if board.is_stalemate() or board.is_variant_draw() or board.is_fivefold_repetition() or board.is_seventyfive_moves():
            result[i] = 0
            break
        board.push(find_best_move(e2, board))
        if board.is_checkmate():
            result[i] = 2
            break
        if board.is_stalemate() or board.is_variant_draw() or board.is_fivefold_repetition() or board.is_seventyfive_moves():
            result[i] = 0
            break
    for e in engines.values():
        e.close()


def main():
    engines = ['stockfish', 'ethereal', 'koivisto']

    for e1_name, e2_name in combinations(engines, 2):
        result = mp.Array('i', [0] * N_TEST)
        threads = []

        t1 = time.time()
        for i in range(N_TEST):
            t = mp.Process(target=duel, args=(e1_name, e2_name, result, i))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()

        e1_wins, e2_wins, draws = 0, 0, 0
        for rc in result:
            if rc == 1:
                e1_wins += 1
            elif rc == 2:
                e2_wins += 1
            else:
                draws += 1
        print(f'Done in {time.time() - t1}s')

        print('\n')
        print(e1_name, 'vs.', e2_name)
        print(e1_wins, ' - ', e2_wins, '             draws:', draws)

    return 0


if __name__ == '__main__':
    main()
