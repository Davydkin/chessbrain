from common import *
from server import Landing
import cherrypy


def main():
    cherrypy.quickstart(Landing(), '/')


if __name__ == '__main__':
    main()
