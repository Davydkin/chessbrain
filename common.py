import chess
import chess.engine
import random
import threading
import time


class Engine:
    def __init__(self):
        self.name = 'unnamed'

    def best_move(self, board):
        raise NotImplementedError

    def close(self):
        pass


class SimpleEngine(Engine):
    def __init__(self, path):
        self.engine = chess.engine.SimpleEngine.popen_uci(path)
        self.engine.configure({"Threads": 8})

    def best_move(self, board):
        info = self.engine.analyse(board, chess.engine.Limit(depth=30, time=10))
        return info['pv'][0]

    def close(self):
        self.engine.close()


class EngineStockfish(SimpleEngine):
    def __init__(self):
        #super().__init__('engines/stockfish_14.1_linux_x64/stockfish_14.1_linux_x64')
        super().__init__('engines/Stockfish/src/stockfish')
        self.name = 'Stockfish'


class EngineEthereal(SimpleEngine):
    def __init__(self):
        super().__init__('engines/Ethereal-13.00/src/Ethereal')
        self.name = 'Ethereal'


class EngineKoivisto(SimpleEngine):
    def __init__(self):
        super().__init__('engines/Koivisto/bin/Koivisto_8.6-x64-linux-native')
        self.name = 'Koivisto'


class EngineLc0(SimpleEngine):
    def __init__(self):
        super().__init__('engines/lc0/build/release/lc0')
        self.name = 'Lc0'


class EngineRandom(Engine):
    def __init__(self):
        self.engines = {
            'stockfish': EngineStockfish(),
            'lc0': EngineLc0(),
        }
        self.name = 'Domran'

    def _engine_calc(self, engine, board, i, result):
        t1 = time.time()
        move = engine.best_move(board)
        dt = time.time() - t1
        result[i] = {'move': move, 'time': dt}

    def best_move(self, board):
        tt = time.time()
        result = [{}] * len(self.engines)
        threads = []
        for i, engine in enumerate(self.engines.values()):
            t = threading.Thread(target=self._engine_calc, args=(engine, board, i, result))
            threads.append(t)
            t.start()
        for t in threads:
            t.join()
        slow_results = []
        fast_results = []
        for item in result:
            if item['time'] > 8:
                slow_results.append(item['move'])
            else:
                fast_results.append(item['move'])
        if len(fast_results) > 0:
            return random.choice(fast_results)
        return random.choice(slow_results)

    def close(self):
        for e in self.engines.values():
            e.close()


class GameSession:
    def __init__(self):
        self.engine = chess.engine.SimpleEngine.popen_uci('engines/Stockfish/src/stockfish')
        self.engine.configure({"Threads": 8})
        self.engine2 = chess.engine.SimpleEngine.popen_uci('engines/lc0/build/release/lc0')
        self.engine2.configure({"Threads": 8})
        self.board = chess.Board()
        self.last_ping = time.time()
        self.analysis = self.engine.analysis(self.board, limit=chess.engine.Limit(depth=35))
        self.analysis2 = self.engine2.analysis(self.board, limit=chess.engine.Limit(depth=35))

    def cancel_move(self):
        self.board.pop()
        self.board.pop()
        self.analysis = self.engine.analysis(self.board)
        self.analysis2 = self.engine2.analysis(self.board)
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
            'board': self.board.epd()
        }
        return result_dict

    def board_push(self, move_uci):
        self.board.push(move_uci)
        self.analysis = self.engine.analysis(self.board)
        self.analysis2 = self.engine2.analysis(self.board)

    def player_move(self, move_string):
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
            'board': self.board.epd()
        }
        try:
            move_uci = chess.Move.from_uci(move_string)
            if move_uci in self.board.legal_moves:
                self.board_push(move_uci)
                if self.board.is_checkmate():
                    result_dict['message'] = 'Шах и мат'
                    result_dict['checkmate'] = True
                    result_dict['game_end'] = True
                elif self.board.is_game_over():
                    result_dict['message'] = 'Игра окончена'
                    result_dict['checkmate'] = False
                    result_dict['game_end'] = True
                else:
                    time.sleep(2)
                    engine_move = self.analysis.info['pv'][0]
                    engine_move2 = self.analysis2.info['pv'][0]
                    self.board_push(random.choice([engine_move, engine_move2]))
                    if self.board.is_checkmate():
                        result_dict['message'] = 'Шах и мат'
                        result_dict['checkmate'] = True
                        result_dict['game_end'] = True
                    elif self.board.is_game_over():
                        result_dict['message'] = 'Игра окончена'
                        result_dict['checkmate'] = False
                        result_dict['game_end'] = True
            else:
                raise ValueError
        except ValueError:
            result_dict['message'] = 'Недопустимый ход'
        result_dict['board'] = self.board.epd()
        return result_dict

    def init_player_is_white(self):
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
            'board': self.board.epd()
        }
        return result_dict

    def init_player_is_black(self):
        result_dict = {
            'message': '',
            'checkmate': False,
            'game_end': False,
            'board': self.board.epd()
        }
        time.sleep(2)
        engine_move = self.analysis.info['pv'][0]
        engine_move2 = self.analysis2.info['pv'][0]
        self.board_push(random.choice([engine_move, engine_move2]))
        result_dict['board'] = self.board.epd()
        return result_dict

    def close(self):
        self.engine.close()
        self.engine2.close()
